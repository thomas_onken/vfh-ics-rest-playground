// BASE SETUP
// =============================================================================

// Benötigte Pakete einbinden
var express    = require('express');
var bodyParser = require('body-parser');
var app        = express();
var morgan     = require('morgan');

// Konfiguration app
app.use(morgan('dev')); // log requests to the console

// Konfiguration body parser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080; // set our port

// MongoDB SETUP
var mongoose   = require('mongoose');
mongoose.connect('mongodb://mdb-tester:mdb-tester123@ds117156.mlab.com:17156/tomyo-cloud-mdb-test'); // connect to our database

// Handle the connection event
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function() {
  console.log("DB connection alive");
});

// Messages lives here
var Message = require('./app/models/message');

// ROUTES FOR OUR API
// =============================================================================

// create our router
var router = express.Router();

// middleware to use for all requests
router.use(function(req, res, next) {
	// do logging
	console.log('Something is happening.');
	next();
});

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
	res.json({ message: 'hooray! welcome to our api!' });	
});

// on routes that end in /messages
// ----------------------------------------------------
router.route('/messages')

	// create a message (accessed at POST http://localhost:8080/messages)
	.post(function(req, res) {
		
		var msg = new Message();		// create a new instance of the Message model
		msg.author = req.body.author;   // set the author 
		msg.message = req.body.message; // set the message
		msg.save(function(err) {
			if (err) {
				res.send(err);
			} else {
				res.json({ status: 'Message created!' });
			}
		});

		
	})

	// get all the messages (accessed at GET http://localhost:8080/api/messages)
	.get(function(req, res) {
		Message.find(function(err, messages) {
			if (err) {
				res.send(err);
			} else {
				res.json(messages);
			}
		});
	});

// on routes that end in /messages/:message_id
// ----------------------------------------------------
router.route('/messages/:message_id')

	// get the message with that id
	.get(function(req, res) {
		Message.findById(req.params.message_id, function(err, msg) {
			if (err) {
				res.send(err);
			} else {
				res.json(msg);
			}
		});
	})

	// update the message with this id
	.put(function(req, res) {
		Message.findById(req.params.message_id, function(err, msg) {

			if (err) {
				res.send(err);
			} else {
				req.body.author ? msg.author = req.body.author : null;
				req.body.message ? msg.message = req.body.author : null;
				msg.save(function(err) {
					if (err) {
						res.send(err);
					} else {
						res.json({ status: 'Message updated!' });
					}
				});
			}
		});
	})

	// delete the message with this id
	.delete(function(req, res) {
		Message.remove({
			_id: req.params.message_id
		}, function(err, message) {
			if (err) {
				res.send(err);
			} else {
				res.json({ status: 'Successfully deleted' });
			}
		});
	});


// REGISTER OUR ROUTES -------------------------------
app.use('/api', router);

// REGISTER OUR STATIC CONTENT -----------------------
app.use(express.static('./app/static'));

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);
