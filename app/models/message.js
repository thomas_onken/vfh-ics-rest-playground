var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var notBlankValidator = function(stringValue) {
	return stringValue.length() > 0;
};

var MessageSchema   = new Schema({
	author: {type: String, required: true, validator: notBlankValidator},
	message: {type: String, required: true, validator: notBlankValidator}
});

module.exports = mongoose.model('Message', MessageSchema);